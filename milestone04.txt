Q.Modify the program created in Milestone 3 to behave as follows:

When the program starts the program will present the user with the following main menu:

1. Add an element to the Array the application should not allow the user to enter the same number twice
2. Search if an element exists in the array
3. Remove an element from the array
4. Sort the array
5. Print all elements in the array
6. Exit
code:
#include<stdio.h>
//#define n 10
int main(){
    int a[10],n,c,i,j,k=0,e,y,z,flag=1;
	printf("enter the size of array: "); scanf("%d",&n);
	for(i=0;i<n;i++){a[i]='\0';}
    while(flag==1){printf("\n1. Add an element to the Array the application should not allow the user to enter the same number twice \n2. Search if an element exists in the array \n3. Remove an element from the array \n4. Sort the array \n5. Print all elements in the array \n6. Exit \nEnter a choice: ");
        scanf("%d",&c);
    
        
    switch(c)
    {   case 1:  printf("\n Add an element: ");
                scanf("%d",&e);
                if(k==n)
                    {printf("array is full");
                    break;}
				y=1;	
                if(y==1){
					//printf("1 here");
				
                    for(i=0;i<n;i++){
                    if(a[i]==e){
                        printf("element already exists");
						y=0;}	
						}
                    }
                if(y==1) 
                {	//printf("2 here");
					a[k]=e;
                    k++;    
                    printf("element inserted successfully");
                }
        break;
    
        case 2: if(k==0)
                {printf("\n array is empty");
                break;}
                printf("\n enter an element to check if it exist in the array: ");
                scanf("%d",&e);
                for(i=0;i<n;i++){
                    if(a[i]==e)
                        {
                        //i=(i+1);
                        printf("\n element %d exist in array at %d position",e,++i);
                        z=1;
                        }
                    }   
                    if(z==0)
                    printf("\n element %d do not exist in array",e);
                    break;
        
        case 3: if(k==0)
                {printf("\n array is empty");
                break;}
                for( i = 0; i < n; i++){
                    printf("\t%d",a[i]);
                    } 
            
                printf("\n enter the element which you want to remove: ");
                scanf("%d",&e);
                for(i=0;i<n;i++){
                    if(a[i]==e)
                    {
                        for(j=i;j<n;j++){
                            a[j]=a[j+1];
                        }
                    }
                }
				k=k-1;
                for( i = 0; i < k; i++){
                    printf("\t%d",a[i]);
                    }   
        break;
        
        case 4: if(k==0)
                {printf("\n array is empty");
                break;}
                printf("\n1. ascending\n2. descending\nenter choice");
                scanf("%d",&c);
                switch(c){

                    case 1: 
                    for (i = 0; i < n; i++) 
                            {
                                for (j = i + 1; j < k; j++) 
                                {
                                    if (a[i] > a[j]) 
                                    {
                                        e = a[i];
                                        a[i] = a[j];
                                        a[j] = e;
                                    }
                                }
                            }
                    break;          

                    case 2: 
                            for (i = 0; i < n; i++) 
                            {
                                for (j = i + 1; j < k; j++) 
                                {
                                    if (a[i] < a[j]) 
                                    {
                                        e = a[i];
                                        a[i] = a[j];
                                        a[j] = e;
                                    }
                                }
                            }
                    break;
                }
                for( i = 0; i < k; i++){
                    printf("\t%d",a[i]);
                    }   
        break;
        
        case 5: if(k==0)
                {printf("\n array is empty");
                break;}
                for( i = 0; i < k; i++){
                    printf("\t%d",a[i]);
                    }
        break;
        
        case 6: flag=2;
        break;
        
        default: printf("\n enter a valid choice");
        break;
        }   
    }
	return 0;
}

output:
enter the size of array: 3

1. Add an element to the Array the application should not allow the user to enter the same number twice
2. Search if an element exists in the array
3. Remove an element from the array
4. Sort the array
5. Print all elements in the array
6. Exit
Enter a choice: 1

 Add an element: 5
element inserted successfully
1. Add an element to the Array the application should not allow the user to enter the same number twice
2. Search if an element exists in the array
3. Remove an element from the array
4. Sort the array
5. Print all elements in the array
6. Exit
Enter a choice: 1

 Add an element: 9
element inserted successfully
1. Add an element to the Array the application should not allow the user to enter the same number twice
2. Search if an element exists in the array
3. Remove an element from the array
4. Sort the array
5. Print all elements in the array
6. Exit
Enter a choice: 7

 enter a valid choice
1. Add an element to the Array the application should not allow the user to enter the same number twice
2. Search if an element exists in the array
3. Remove an element from the array
4. Sort the array
5. Print all elements in the array
6. Exit
Enter a choice: 4

1. ascending
2. descending
enter choice1
        5       9
1. Add an element to the Array the application should not allow the user to enter the same number twice
2. Search if an element exists in the array
3. Remove an element from the array
4. Sort the array
5. Print all elements in the array
6. Exit
Enter a choice:6
